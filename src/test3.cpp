#include <iostream>
#include "core/core.hpp"
#include "pe/newton.hpp"
#include "pe/leapfrog.hpp"
#include <thread>
#include <unistd.h>

using namespace std;

int main()
{
    core::Simulation *sim = new core::Simulation;

    pe::NewtDynBody *b1 = new pe::NewtDynBody(sim); // star
    pe::NewtDynBody *b2 = new pe::NewtDynBody(sim); // planet 1
    pe::NewtDynBody *b3 = new pe::NewtDynBody(sim); // planet 2

    // parameters
    b1->Mi = b1->Mg = 2e30;
    b2->Mi = b2->Mg = 1e24;
    b3->Mi = b3->Mg = 1e24;

    // initial conditions
    b1->r = {0,0,0};
    b2->r = {-1.5e11,-1.5e10,0};
    b3->r = {1.5e11,-1.5e11,0};

    b1->v = {0,-10e3,0};
    b2->v = {0,30e3,0};
    b3->v = {-10e3,30e3,0};

    sim->bodies.push_back(b1);
    sim->bodies.push_back(b2);
    sim->bodies.push_back(b3);

    thread simLoop([&](){sim->start();});
    thread simStop([&](){
                       usleep(30e6);
                       sim->stop();
                   });

    simLoop.join();
    simStop.join();

    delete b1;
    delete b2;
    delete b3;
    delete sim;

    return 0;
}
