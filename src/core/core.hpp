#ifndef CORE_H
#define CORE_H

#include <vector>
#include <iostream>
#include "core_math.hpp"

namespace pe
{
    // We declare these here to prevent need to cross-include headers
    class DynBody;
    class Integrator;
}

namespace core
{
    class Simulation;
    class Body;

    /**
     * Base class for handling a simulation instance.
     */
    class Simulation
    {
    public:
        flt Tsim; ///< Simulation elapsed time in seconds.
        flt Tmjd; ///< Current MJD time (decimal days).

        std::vector<pe::DynBody*> bodies; ///< Bodies currently present in the simulation.

        pe::Integrator* integ = nullptr; ///< Integrator instance associated with current Simulation.

        // friend ostream & operator << (ostream & out, Simulation & sim); ///< Store simulation to stream
        // friend istream & operator >> (istream & in, Simulation & sim);  ///< Load simulation from stream

    public:
        Simulation();
        ~Simulation();
        void start(); ///< Wrapper around Integrator::start().
        void pause();
        void stop(); ///< Wrapper around Integrator::stop().
    };

    /**
     * Base class representing each object in the simulation.
     */
    class Body {
    public:
        Body(Simulation* parentSim);
        flt properTime; ///< Body's proper time (session persistent)
        Vec r, ///< Position of the center of mass.
            v, ///< Velocity of the center of mass.
            ang, ///< Euler angles (with respect to the "absolute" frame).
            angVel; ///< Angular velocity.
        Simulation* parentSim; ///< Simulation instance this body belongs to.
    };
}

#endif /* CORE_H */
