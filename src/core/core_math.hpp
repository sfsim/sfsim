#ifndef CORE_MATH_H
#define CORE_MATH_H

#include <valarray>
#include <cmath>

typedef __float128 flt;

/**
 * Class for n-dimensional normed vectors.
 * It's just a valarray equipped with euclidean norm.
 */
class Vec : public std::valarray<flt>
{
public:
    using std::valarray<flt>::valarray;
    using std::valarray<flt>::operator=;

    flt norm();
    flt norm2();
};

#endif /* CORE_MATH_H */
