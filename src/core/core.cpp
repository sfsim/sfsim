#include "core.hpp"
#include "../pe/leapfrog.hpp"

core::Simulation::Simulation() :
    Tsim(0)
{}

core::Simulation::~Simulation()
{
    delete integ;
}

void core::Simulation::start()
{
    integ = new pe::LeapFrog(this);
    integ->start();
}

void core::Simulation::stop()
{
    integ->stop();
}

core::Body::Body(Simulation* parentSim)
{
    this->parentSim = parentSim;
}
