#include "core_math.hpp"
#include <iostream>

/**
 * \brief Returns the (euclidean) norm of the vector squared.
 */
flt Vec::norm2()
{
    return ((*this)*(*this)).sum();
}

/**
 * \brief Returns the (euclidean) norm of the vector.
 */
flt Vec::norm()
{
    return sqrt(norm2());
}
