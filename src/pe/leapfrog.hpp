#ifndef LEAPFROG_H
#define LEAPFROG_H

#include "pe.hpp"

namespace pe
{
    /**
     * Integrates forces (whatever their origin) given by DynBody with classical \f$ \vec{F} = m \vec{a} \f$ using leapfrog algorithm
     * \f[
       \begin{cases}
         \vec{r}(t+\mathrm{d}t) = \vec{r}(t) + \vec{v}\left(t+\frac{\mathrm{d}t}{2}\right) \mathrm{d}t \\
         \vec{v}\left(t+\frac{3}{2}\mathrm{d}t\right) = \vec{v}(t+\frac{\mathrm{d}t}{2}) + \frac{\vec{F}}{m}\left(\vec{r}(t+\mathrm{d}t),t+\mathrm{d}t\right) \mathrm{d}t
       \end{cases}
       \f]
       When the simulation is running, positions and velocities are out of phase by a time interval \f$\mathrm{d}t/2\f$. When interrupted, a half step is performed with Euler's method to get rid of that phase difference (which is similarly introduced before leapfrog starts).
     */
    class LeapFrog : public Integrator {
    private:
        flt dt = 10; ///< internal integration interval in seconds, set equal to delta at the beginning of every step
        void updateDt();
    public:
        flt delta = 10; ///< integration interval in seconds, to be changed at will
        using Integrator::Integrator;
        std::vector<Vec> computeForces(); ///< Gets total force via DynBody::calculateNetForce().
        void start() override; ///< Start simulation loop.
        void stop() override; ///< Trigger simulation stop.
        // void computeCoords();
        // void computeVelocity();
        // void finish();
        // int step();
        // int run();
        // LeapFrog (Simulation* simulation, const flt & warp = 1, flt dt0 = 1) {
        // parentSimulation = simulation; maxWarp = warp; dt = dt0;
        // }
    };
}
#endif /* LEAPFROG_H */
