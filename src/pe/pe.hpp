#ifndef PE_H
#define PE_H

#include <armadillo>
#include "../core/core.hpp"

typedef arma::dmat::fixed<3,3> mat;

namespace pe
{
    /**
     * Contains dynamical properties of a body.
     */
    class DynBody : public core::Body
    {
    public:
        // Inertial properties.
        flt Mi; ///< Inertial mass.
        mat I; ///< Inertia tensor.

        // Electrical properties
        flt q; ///< Electric charge.
        Vec qDip; ///< Electric dipole.
        mat qQuadr; ///< Electric quadrupole.
        Vec mDip; ///< Magnetic dipole.

        // Gravitational properties.
        flt Mg; ///< Gravitational mass.
        mat mQuadr; ///< Gravitational quadrupole.

        // virtual pair<Vec,Vec> calculateForces(const flt & Tsim); //returns Force and Torque on the body

        using core::Body::Body;

        virtual Vec calculateNetForce() = 0; ///< Returns net force acting on the body at core::Simulation::Tsim.
        virtual Vec calculateNetTorque() = 0; ///< Returns net torque acting on the body at core::Simulation::Tsim.
    };

    /**
     * Base class for a numerical solver
     */
    class Integrator
    {
    protected:
        core::Simulation* parentSim;
        volatile bool running = false;
    public:
        virtual void start() = 0;
        // virtual void step() = 0;
        virtual void stop() = 0;

        // flt maxWarp; //Aims at reaching this simulation speed

        Integrator (core::Simulation* parentSim);
        // parentSimulation = simulation;
        // maxWarp = warp;
        // }
        // virtual int run(); //Run function to be called by thread launcher

    };
}

#endif /* PE_H */
