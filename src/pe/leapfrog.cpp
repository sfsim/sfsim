#include "leapfrog.hpp"
#include <fstream>
#include <string>
#include <vector>

/**
 * Get vector with all net forces (in the same order as in core::Simulation::bodies)
 */
std::vector<Vec> pe::LeapFrog::computeForces()
{
    std::vector<Vec> forces;
    for (auto body : parentSim->bodies) {
        forces.push_back(body->calculateNetForce());
    }
    return forces;
}

/**
 * Polished update of dt
 */
void pe::LeapFrog::updateDt()
{
    unsigned nBodies = parentSim->bodies.size();
    
    // Forward Euler half step for position with old dt
    for (unsigned i=0; i<nBodies; i++)
        parentSim->bodies[i]->r
            += parentSim->bodies[i]->v * dt/2;
    
    dt = delta; // Update dt
    
    // Forward Euler half step for velocity with new dt
    auto forces = computeForces();
    for (unsigned i=0; i<nBodies; i++)
        parentSim->bodies[i]->v
            += forces[i]/parentSim->bodies[i]->Mi * dt/2; // Forces at time t
    parentSim->Tsim += dt/2; // Body's computeForces() might use simulation time
}

void pe::LeapFrog::start()
{
    auto forces = computeForces();
    unsigned nBodies = forces.size();

    // Forward Euler half step for velocity
    for (unsigned i=0; i<nBodies; i++)
        parentSim->bodies[i]->v
            += forces[i]/parentSim->bodies[i]->Mi * dt/2; // Forces at time t
    parentSim->Tsim += dt/2; // Body's computeForces() might use simulation time

    // Open vector of output files: one for each body
    std::vector<std::ofstream> f(nBodies);
    for (unsigned i=0; i<nBodies; i++)
    {
        std::string filename = "out"+std::to_string(i)+".txt";
        f[i].open(filename);
    }

    // Main simulation loop (leapfrog)
    running = true;
    unsigned stepcounter=0;

    while (running) {

        if (dt != delta)
            updateDt();

        if ((stepcounter++ % 128) == 0) // Print to file one step every 128
        {
            for (unsigned i=0; i<nBodies; i++)
            {
                f[i] << double(parentSim->Tsim)
                     << " " << double(parentSim->bodies[i]->r[0])
                     << " " << double(parentSim->bodies[i]->r[1])
                     << " " << double(parentSim->bodies[i]->r[2]) << '\n';
            }
        }

        for (unsigned i=0; i<nBodies; i++)
            parentSim->bodies[i]->r
                += parentSim->bodies[i]->v * dt; // v at time t+dt
        forces = computeForces(); // Forces at time t+dt (depend on position, which is now at time t+dt)

        for (unsigned i=0; i<nBodies; i++)
            parentSim->bodies[i]->v
                += forces[i]/parentSim->bodies[i]->Mi * dt; // Forces at time t+dt
        parentSim->Tsim += dt; // Update simulation time
    }

    // Close output files
    for (unsigned i=0; i<nBodies; i++)
    {
        f[i].close();
    }

    // Forward Euler half step for position
    for (unsigned i=0; i<nBodies; i++)
        parentSim->bodies[i]->r
            += parentSim->bodies[i]->v * dt/2;
}

void pe::LeapFrog::stop()
{
    running = false;
}
