#ifndef NEWTON_H
#define NEWTON_H

#include "pe.hpp"

const flt G = 6.6743015e-11;

namespace pe
{
    class NewtDynBody : public DynBody
    {
    public:
        using pe::DynBody::DynBody;
        Vec calculateNetForce() override;
        Vec calculateNetTorque() override;
    };
}

#endif /* NEWTON_H */
