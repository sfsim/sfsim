#include "newton.hpp"
#include <cmath>

/**
 * Gravitational force calculation.
 */
Vec pe::NewtDynBody::calculateNetForce()
{
    Vec netForce = {0,0,0};
    for (auto body : parentSim->bodies) {
        // Do not compute forces of the body on itself.
        if (body == this)
            continue;
        netForce += (body->r - this->r) * G * this->Mg * body->Mg / pow((Vec(body->r - this->r)).norm(), 3); // TODO: "Cast" to Vec should not be required.
    }
    return netForce;
}

/**
 * Dummy torque calculation.
 */
Vec pe::NewtDynBody::calculateNetTorque()
{
    return Vec({0,0,0});
}
