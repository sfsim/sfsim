#include <iostream>
#include "core/core.hpp"
#include "pe/newton.hpp"
#include "pe/leapfrog.hpp"
#include <thread>
#include <unistd.h>

using namespace std;

int main()
{
    core::Simulation *sim = new core::Simulation;

    pe::NewtDynBody *b1 = new pe::NewtDynBody(sim);
    pe::NewtDynBody *b2 = new pe::NewtDynBody(sim);

    // parameters
    b1->Mi = b1->Mg = 2e21;
    b2->Mi = b2->Mg = 1e21;

    // initial conditions
    b1->r = {0,133333333,0};
    b2->r = {0,-266666666,0};

    b1->v = {-7.457,0,0};
    b2->v = {14.916,0,0};

    sim->bodies.push_back(b1);
    sim->bodies.push_back(b2);

    thread simLoop([&](){sim->start();});
    thread simStop([&](){
                       usleep(30e6);
                       sim->stop();
                   });

    simLoop.join();
    simStop.join();

    delete b1;
    delete b2;
    delete sim;

    return 0;
}
