#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

pathList = ['../../build/out1.txt', '../../build/out2.txt']

ax = plt.figure().add_subplot(111, projection='3d')

for path in pathList:
    t, x, y, z = np.loadtxt(path, unpack=True)
    ax.plot(xs=x, ys=y, zs=z)

ax.set_xlabel("x")
ax.set_ylabel("y")
ax.set_zlabel("z")


plt.show()
