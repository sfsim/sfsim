#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

pathList = ['../../build/out1.txt',
            '../../build/out2.txt',
            '../../build/out3.txt']

ax = plt.figure().add_subplot(111, aspect='equal')

for path in pathList:
    t, x, y, z = np.loadtxt(path, unpack=True)
    ax.plot(x, y)

ax.set_xlabel("x")
ax.set_ylabel("y")

plt.show()
