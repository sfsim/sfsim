# sfsim

## Requirements
To compile from source you're going to need:

 - g++
 - cmake
 - make
 - libarmadillo-dev

To compile docs you're going to need:

 - doxygen
 - graphviz (needed to build dependency graphs)

## Build
To build:
```
$ mdir build
$ cd build
$ cmake -G "Unix Makefiles" ..
$ make
```

To build docs:
```
cd doc
doxygen Doxyfile
```
Then open `doc/html/index.html` with your browser.
